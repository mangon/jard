package jard.drone;

import de.yadrone.base.IARDrone;
import de.yadrone.base.utils.ARDroneUtils;
import jard.pilot.Pilot;

/**
 * Created by fc on 10Apr17.
 */
public class Cockpit {

    boolean connected;
    Pilot pilot;
    IARDrone drone;
    boolean running;
    boolean pilotAccepted;
    boolean flying;
    boolean hovering;
    boolean landed;

    public IARDrone getDrone() {
        return drone;
    }

    public void setDrone(IARDrone drone) {
        this.drone = drone;
    }

    public boolean hasDrone() {
        return drone != null;
    }

    public boolean isConnected() {
        if (hasDrone()) {
            connected = drone.getConfigurationManager().isConnected();
        } else {
            connected = false;
        }
        return connected;
    }

    void tryConnect() {
        try {
            drone.getCommandManager().connect(ARDroneUtils.CONTROL_PORT);
        } catch (Exception ignored) {}
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public boolean hasPilot() {
        return pilot != null;
    }

    public Cockpit() {
        connected = false;
        pilot = null;
        drone = null;
    }

    // TODO: maybe...
    public void run() {
        while (running) {
            if (!isConnected()) {
                tryConnect();
            } else if (!hasPilot()) {
                if (hovering) {
                }
            }
        }
    }
}
