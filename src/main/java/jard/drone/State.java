package jard.drone;

/**
 * Created by fc on 10Apr17.
 */
public class State {
    private float battery;
    private float voltage;
    private float magneto;
    private float wifiQuality;
    private float windAngle;
    private float windSpeed;

    private float altitude;
    private float acceleroX;
    private float acceleroY;
    private float acceleroZ;

    private float gyroX;
    private float gyroY;
    private float gyroZ;

    public float getGyroX() {
        return gyroX;
    }

    public void setGyroX(float gyroX) {
        this.gyroX = gyroX;
    }

    public float getGyroY() {
        return gyroY;
    }

    public void setGyroY(float gyroY) {
        this.gyroY = gyroY;
    }

    public float getGyroZ() {
        return gyroZ;
    }

    public void setGyroZ(float gyroZ) {
        this.gyroZ = gyroZ;
    }


    public float getBattery() {
        return battery;
    }

    public void setBattery(float battery) {
        this.battery = battery;
    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public float getMagneto() {
        return magneto;
    }

    public void setMagneto(float magneto) {
        this.magneto = magneto;
    }

    public float getWifiQuality() {
        return wifiQuality;
    }

    public void setWifiQuality(float wifiQuality) {
        this.wifiQuality = wifiQuality;
    }

    public float getWindAngle() {
        return windAngle;
    }

    public void setWindAngle(float windAngle) {
        this.windAngle = windAngle;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public float getAcceleroX() {
        return acceleroX;
    }

    public void setAcceleroX(float acceleroX) {
        this.acceleroX = acceleroX;
    }

    public float getAcceleroY() {
        return acceleroY;
    }

    public void setAcceleroY(float acceleroY) {
        this.acceleroY = acceleroY;
    }

    public float getAcceleroZ() {
        return acceleroZ;
    }

    public void setAcceleroZ(float acceleroZ) {
        this.acceleroZ = acceleroZ;
    }
}
