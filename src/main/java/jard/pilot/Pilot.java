package jard.pilot;

import jard.drone.Cockpit;

/**
 * Created by fc on 10Apr17.
 */
public class Pilot {
    boolean running;
    boolean ready;
    Cockpit plane;
    String aslProgram;

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isReady() {
        return ready;
    }

    public void setPlane(Cockpit plane) {
        this.plane = plane;
    }

    public String getAslProgram() {
        return aslProgram;
    }

    public void setAslProgram(String aslProgram) {
        this.aslProgram = aslProgram;
    }

    public boolean hasProgram() {
        return aslProgram != null;
    }

    public Pilot() {
        running = false;
        ready = false;
        aslProgram = null;
        plane = null;
    }
}
