package jard.pilot;

import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.command.LEDAnimation;
import de.yadrone.base.navdata.*;

import jason.architecture.AgArch;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Agent;
import jason.asSemantics.TransitionSystem;
import jason.asSyntax.Literal;
import jason.util.Config;

import jard.drone.State;

/**
 *
 * Created by fc on 27Mar17.
 */
public class BasePilot extends AgArch implements Runnable {
    private static void control(IARDrone drone, ActionExec action) {
        CommandManager cm = drone.getCommandManager();
        switch (action.getActionTerm().getFunctor()) {
            //
            // Basic motion
            //
            case "up":
                cm.up(drone.getSpeed()); break;
            case "down":
                cm.down(drone.getSpeed()); break;
            case "forward":
                cm.forward(drone.getSpeed()); break;
            case "backward":
                cm.backward(drone.getSpeed()); break;
            case "goLeft":
                cm.goLeft(drone.getSpeed()); break;
            case "goRight":
                cm.goRight(drone.getSpeed()); break;
            //
            // Spin
            //
            case "spinLeft":
                cm.spinLeft(drone.getSpeed()); break;
            case "spinRight":
                cm.spinRight(drone.getSpeed()); break;
            //
            // Standing still
            //
            case "freeze":
                cm.freeze(); break;
            case "hover":
                cm.hover(); break;
            //
            // Start / Stop
            //
            case "start":
                drone.start(); break;
            case "flatTrim":
                cm.flatTrim(); break;
            case "waitFor":
                int wf = Integer.parseInt(action.getActionTerm().getTerm(0).toString());
                cm.waitFor(wf);
                break;
            case "stop":
                cm.stop(); break;
            case "reset":
                drone.reset(); break;
            //
            // Takeoff / landing
            //
            case "takeOff":
                cm.takeOff(); break;
            case "land":
                cm.landing(); break;
            //
            // Throttling
            //
            case "setSpeed":
                int speedValue = Integer.parseInt(action.getActionTerm().getTerm(0).toString());
                drone.setSpeed(speedValue); break;
            //
            //  Leds
            //
            case "blink":
                float freq = Float.parseFloat(action.getActionTerm().getTerm(0).toString());
                int duration = Integer.parseInt(action.getActionTerm().getTerm(1).toString());
                cm.setLedsAnimation(LEDAnimation.BLINK_ORANGE, freq, duration);
                break;
            //
            //  Cameras
            //
            case "setVerticalCamera":
                drone.setVerticalCamera();
                break;
            case "setHorizontalCamera":
                drone.setHorizontalCamera();
                break;
            case "toggleCamera":
                drone.toggleCamera();
                break;
            //
            // Default
            //
            default:
                logger.log(Level.WARNING, String.format(
                        "Drone can't do [%s]", action));
        }
    }
    private static Logger logger = Logger.getLogger(BasePilot.class.getName());

    private IARDrone drone;

    private State droneState;
    private boolean _isRunning;
    private List<String> percepts;

    public BasePilot(String prog) {
        this.drone = null;
        droneState = new State();
        percepts = Collections.synchronizedList(new ArrayList<String>());
        try {
            // Disable the Web Mind Inspector
            Config.get().setProperty(Config.START_WEB_MI, "false");
            Agent ag = new Agent();
            new TransitionSystem(ag, null, null, this);
            ag.initAg();
            ag.parseAS(new StringReader(prog));
            _isRunning = false;
            // DEBUG
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Init error", e);
        }
    }

    public void setDrone(IARDrone drone) {
        this.drone = drone;
    }

    public State getDroneState() {
        return droneState;
    }

    public void run() {
        _isRunning = true;
        bindDroneState();
        try {
            while (isRunning()) {
                perceptDroneState();
                //
                // calls the Jason engine to perform one reasoning cycle
                //
                getTS().reasoningCycle();
                if (getTS().canSleep()) {
                    sleep();
                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Run error", e);
        }
    }

    public String getAgName() {
        return "earhart";
    }

    public void addPercept(String p) {
        percepts.add(p);
    }

    private void perceptDroneState() {
        addPercept(String.format("droneConnected(%s)", drone != null));
        addPercept(String.format("droneBattery(%.0f)", droneState.getBattery()));
        addPercept(String.format("droneVoltage(%.0f)", droneState.getVoltage()));
        addPercept(String.format("droneMagneto(%.0f)", droneState.getMagneto()));
        addPercept(String.format("droneWifi(%.0f)", droneState.getWifiQuality()));
        addPercept(String.format("droneWindAngle(%.0f)", droneState.getWindAngle()));
        addPercept(String.format("droneWindSpeed(%.0f)", droneState.getWindSpeed()));
        addPercept(String.format("droneAltitude(%.0f)", droneState.getAltitude()));
        addPercept(String.format("droneAcceleroX(%f)", droneState.getAcceleroX()));
        addPercept(String.format("droneAcceleroY(%f)", droneState.getAcceleroY()));
        addPercept(String.format("droneAcceleroZ(%f)", droneState.getAcceleroZ()));
        addPercept(String.format("droneGyroX(%f)", droneState.getGyroX()));
        addPercept(String.format("droneGyroY(%f)", droneState.getGyroY()));
        addPercept(String.format("droneGyroZ(%f)", droneState.getGyroZ()));
    }

    private void bindDroneState() {
        if (drone != null && drone.getConfigurationManager().isConnected()) {
            NavDataManager nd = drone.getNavDataManager();
            //
            //  Battery
            //
            nd.addBatteryListener(new BatteryListener() {
                @Override
                public void batteryLevelChanged(int percentage) {
                    droneState.setBattery(percentage);
                }

                @Override
                public void voltageChanged(int vbat_raw) {
                    droneState.setVoltage(vbat_raw);
                }
            });
            //
            //  Pressure
            //
            nd.addPressureListener(new PressureListener() {
                @Override
                public void receivedKalmanPressure(KalmanPressureData d) {
                    // TODO
                }

                @Override
                public void receivedPressure(Pressure d) {
                    // TODO
                }
            });
            //
            //  Accelero
            //
            nd.addAcceleroListener(new AcceleroListener() {
                @Override
                public void receivedRawData(AcceleroRawData d) {
                    // TODO
                }

                @Override
                public void receivedPhysData(AcceleroPhysData d) {
                    float[] a = d.getPhysAccs();
                    droneState.setAcceleroX(a[0]);
                    droneState.setAcceleroY(a[1]);
                    droneState.setAcceleroZ(a[2]);
                }
            });
            //
            //  Magneto
            //
            nd.addMagnetoListener((MagnetoData d) ->
                droneState.setMagneto(d.getHeadingFusionUnwrapped()));
            //
            //  Wifi
            //
            nd.addWifiListener((long link_quality) ->
                    droneState.setWifiQuality(link_quality));
            //
            //  Wind
            //
            nd.addWindListener((WindEstimationData d) -> {
                float angle = d.getEstimatedAngle();
                float speed = d.getEstimatedSpeed();
                droneState.setWindAngle(angle);
                droneState.setWindSpeed(speed);
            });
            //
            //  Altitude
            //
            nd.addAltitudeListener(new AltitudeListener() {
                @Override
                public void receivedAltitude(int altitude) {
                    // TODO
                }

                @Override
                public void receivedExtendedAltitude(Altitude d) {
                    droneState.setAltitude(d.getObsAlt());
                }
            });
            //
            //
            //
            nd.addGyroListener(new GyroListener() {
                @Override
                public void receivedRawData(GyroRawData d) {

                    // TODO
                    // short[] x = d.getRawGyros();
                }

                @Override
                public void receivedPhysData(GyroPhysData d) {
                    float[] a =d.getPhysGyros();
                    droneState.setGyroX(a[0]);
                    droneState.setGyroY(a[1]);
                    droneState.setGyroZ(a[2]);
                }

                @Override
                public void receivedOffsets(float[] offset_g) {

                }
            });
        }
    }

    // this method just add some perception for the agent
    @Override
    public List<Literal> perceive() {
        List<Literal> l = new ArrayList<>();
        for (String s : percepts) {
            l.add(Literal.parseLiteral(s));
        }
            percepts.clear();
        return l;
    }

    // this method get the agent actions
    @Override
    public void act(ActionExec action) {
        if (drone != null) {
            BasePilot.control(drone, action);
            action.setResult(true);
            actionExecuted(action);
        }
    }

    @Override
    public boolean canSleep() {
        return true;
    }

    public void stop() {
        _isRunning = false;
    }

    @Override
    public boolean isRunning() {
        return _isRunning;
    }

    // a very simple implementation of sleep
    private void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException ignored) {}
    }

    // Not used methods
    // This simple agent does not need messages/control/...
    @Override
    public void sendMsg(jason.asSemantics.Message m) throws Exception {
    }

    @Override
    public void broadcast(jason.asSemantics.Message m) throws Exception {
    }

    @Override
    public void checkMail() {
    }
}
