package jard;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.ResourceBundle;
//
import de.yadrone.base.exception.ARDroneException;
import de.yadrone.base.exception.IExceptionListener;
import jason.asSemantics.ActionExec;
import jason.asSemantics.Event;
import jason.asSemantics.Intention;
import jason.asSemantics.Option;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
//
import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.VideoCodec;
import de.yadrone.base.navdata.*;
//
import jason.asSyntax.Literal;
//
import jard.scale.IndicatorSet;
import jard.pilot.BasePilot;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


public class Controller implements Initializable {

    private Stage stage;

    @FXML
    private Button loadButton;

    @FXML
    private Button saveButton;


    @FXML
    private Button trimButton;

    @FXML
    private ToggleButton autoPilotToggle;

    @FXML
    private TextArea pilotProg;

    @FXML
    private Button emergencyButton;

    @FXML
    private ImageView cameraView;

    @FXML
    private Canvas instrumentsView;

    @FXML
    private ToggleButton connectToggle;

    @FXML
    private AnchorPane mainContainer;

    @FXML
    private TreeView pilotMindStateView;
    //
    private TreeItem pilotMsBeliefs = new TreeItem("Beliefs");
    private TreeItem pilotMsEvents = new TreeItem("Events");
    private TreeItem pilotMsSelEvent = new TreeItem("Selected Event");
    private TreeItem pilotMsOptions = new TreeItem("Options");
    private TreeItem pilotMsSelOption = new TreeItem("Selected Option");
    private TreeItem pilotMsIntentions = new TreeItem("Intentions");
    private TreeItem pilotMsSelIntention = new TreeItem("Selected Intention");
    private TreeItem pilotMsAction = new TreeItem("Action");

    @FXML
    private Label agentFilename;

    @FXML
    private Label flyStatusLabel;
    private String flyStatusStr;

    private GraphicsContext gc;

    private IARDrone drone;
    private BasePilot pilot;

    private IndicatorSet instruments;

    private boolean droneOk = false;
    private boolean pilotOk = false;
    private URL pilotFilename;

    public Controller() {
    }

    public void setScene(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        //
        ////////////////////////
        //
        //  Setup instruments
        //
        setupInstruments();
        //
        ////////////////////////
        //
        //  Setup mindview
        //
        setupPilotMindStateView();
        //
        ////////////////////////
        //
        //  Setup drone
        //
        System.out.println("Setting drone.");
        drone = null;
        setupDrone();
        System.out.printf("Drone set. Connected? %s\n", drone.getNavDataManager().isConnected());
        //
        ////////////////////////
        //
        //  Setup pilot
        //
        System.out.println("Setting pilot.");
        pilot = null;
        setupPilot();
        System.out.println("Pilot set.");
    }
    /*
    *
    *
    *   INSTRUMENTS
    *
    *
    */
    private void setupInstruments() {
        double width = instrumentsView.getWidth();
        double height = instrumentsView.getHeight();
        double p_height = width * 3.0 / 5.0;
        double p_width = height * 5.0 / 3.0;
        if (p_height > height) {
            instrumentsView.setHeight(height);
            instrumentsView.setWidth(p_width);
            instrumentsView.setLayoutX(0.5 * (width - p_width ));
        } else {
            instrumentsView.setHeight(p_height);
            instrumentsView.setWidth(width);
        }
        gc = instrumentsView.getGraphicsContext2D();
        instruments = new IndicatorSet();
    }
    /*
    *
    *
    *   MIND STATE
    *
    *
    */
    private void setupPilotMindStateView() {
        ArrayList<TreeItem> rootMindView = new ArrayList<>();
        //
        pilotMsBeliefs.setExpanded(true);
        rootMindView.add(pilotMsBeliefs);
        //
        rootMindView.add(pilotMsEvents);
        rootMindView.add(pilotMsSelEvent);
        //
        rootMindView.add(pilotMsOptions);
        rootMindView.add(pilotMsSelOption);
        //
        rootMindView.add(pilotMsIntentions);
        rootMindView.add(pilotMsSelIntention);
        //
        rootMindView.add(pilotMsAction);
        pilotMsAction.setExpanded(true);

        TreeItem rootView = new TreeItem("Pilot Mind State");
        rootView.getChildren().addAll(rootMindView);
        rootView.setExpanded(true);
        pilotMindStateView.setRoot(rootView);

    }
    //
    //  Update Mind State View
    //
    private void updatePilotMindStateView() {
        //
        updatePilotBeliefsView();
        updatePilotActionView();
        //
        updatePilotEventsView();
        updatePilotSelEventView();
        //
        updatePilotOptionsView();
        updatePilotSelOptionView();
        //
        updatePilotIntentionsView();
        updatePilotSelIntentionView();
    }
    //
    private void updatePilotEventsView() {
        pilotMsEvents.getChildren().clear();
        Queue<Event> events = pilot.getTS().getC().getEvents();
        if (events.isEmpty()) {
            pilotMsEvents.getChildren().add(new TreeItem<>("(no events)"));
        } else {
            for (Event b : events)
                pilotMsEvents.getChildren().add(new TreeItem<>(b.toString()));
        }
    }
    //
    private void updatePilotSelEventView() {
        pilotMsSelEvent.getChildren().clear();
        Event b = pilot.getTS().getC().getSelectedEvent();
        String bs = "(no event)";
        if (b != null) bs = b.toString();
        pilotMsSelEvent.getChildren().add(new TreeItem<>(bs));
    }
    //
    private void updatePilotBeliefsView() {
        pilotMsBeliefs.getChildren().clear();
        for(Literal b : pilot.getTS().getAg().getBB())
            pilotMsBeliefs.getChildren().add(new TreeItem<>(b.toString()));
    }
    //
    private void updatePilotActionView() {
        pilotMsAction.getChildren().clear();
        ActionExec b = pilot.getTS().getC().getAction();
        String bs = "(no action)";
        if (b != null) bs = b.toString();
        pilotMsAction.getChildren().add(new TreeItem<>(bs));
    }
    //
    private void updatePilotOptionsView() {
        pilotMsOptions.getChildren().clear();
        List<Option> options = pilot.getTS().getC().getApplicablePlans();
        if (options != null) {
            for(Option b : options)
                pilotMsOptions.getChildren().add(new TreeItem<>(b.toString()));
        } else {
            pilotMsOptions.getChildren().add(new TreeItem<>("(no options)"));
        }
    }
    //
    private void updatePilotSelOptionView() {
        pilotMsSelOption.getChildren().clear();
        Option b = pilot.getTS().getC().getSelectedOption();
        String bs = "(no option)";
        if (b != null) bs = b.toString();
        pilotMsSelOption.getChildren().add(new TreeItem<>(bs));
    }
    //
    private void updatePilotIntentionsView() {
        pilotMsIntentions.getChildren().clear();
        Queue<Intention> intentions = pilot.getTS().getC().getIntentions();
        if (intentions.isEmpty()) {
            pilotMsIntentions.getChildren().add(new TreeItem<>("(no intentions)"));
        } else {
            for(Intention b : intentions)
                pilotMsIntentions.getChildren().add(new TreeItem<>(b.toString()));
        }
    }
    //
    private void updatePilotSelIntentionView() {
        pilotMsSelIntention.getChildren().clear();
        Intention b = pilot.getTS().getC().getSelectedIntention();
        String bs = "(no intention)";
        if (b != null) bs = b.toString();
        pilotMsSelIntention.getChildren().add(new TreeItem<>(bs));
    }
    //
    //  Control loop
    //
    void step() {
        instruments.draw(gc);
        agentFilename.setText(pilotFilename.toString());
        flyStatusLabel.setText(flyStatusStr);
        if (pilotOk) {
            if (droneOk) autoPilotToggle.setDisable(false);
            updatePilotMindStateView();
        }

    }
    //
    //  Shutdown
    //
    void shutdown() {
        if (droneOk) {
            drone.stop();
        }
        if (pilotOk) {
            pilot.stop();
        }
        System.out.println("Controller shutdown");
    }
    //
    //  Tracing
    //
    void tempLog(String msg) {
        System.err.print(String.format("LOG: %s\n", msg));
    }

    /*
    *
    *
    *   PILOT
    *
    *
     */
    /**
     *
     */
    private void setupPilot() {
        //
        //  Get program from file
        //
        setupPilotProgram("file:./asl/basepilot.asl");
        bootPilot();
    }

    private void bootPilot() {
        //
        //  Create the pilot
        //
        pilot = new BasePilot(pilotProg.getText());
        pilot.setDrone(drone);
        Thread t = new Thread(() -> pilot.run());
        t.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pilotOk = true;
    }
    /*
    *
    *
    *   DRONE
    *
    *
     */
    private void setupDrone() {
        drone = new ARDrone();
        drone.start();
        droneOk = drone.getConfigurationManager().isConnected();
        if (!droneOk) {
            drone.stop();
            drone.getNavDataManager().close();
            drone.getNavDataManager().stop();
        } else {
            bindDrone(drone);
        }
    }

    private void bindDrone(IARDrone drone) {
        bindPressure(drone);
        bindState(drone);
        bindAltitude(drone);
        bindBattery(drone);
        bindAttitude(drone);
        bindWind(drone);
        bindWifi(drone);
        bindCompass(drone);
        bindVideo(drone);
        drone.setHorizontalCamera();
    }

    private void bindVideo(IARDrone drone) {
        drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P_SLRS);
        drone.getVideoManager().addImageListener(
                (BufferedImage image) -> cameraView.setImage(
                        SwingFXUtils.toFXImage(image, null) )
        );
    }

    private void bindAttitude(IARDrone drone) {
        drone.getNavDataManager().addAttitudeListener(
            new AttitudeListener() {
                public void attitudeUpdated(float pitch, float roll, float yaw) {
                    attitudeUpdated(pitch, roll);
                    instruments.setValueYaw(0.001 * yaw);
                }

                public void attitudeUpdated(float pitch, float roll) {
                    instruments.setValuePitch(0.001 * pitch);
                    instruments.setValueRoll(-0.001 * roll);
                }

                public void windCompensation(float pitch, float roll) {
                    instruments.setValueWindPitch(0.001 * pitch);
                    instruments.setValueWindRoll(0.001 * roll);
                }
            }
        );
    }

    private void bindBattery(IARDrone drone) {
        drone.getNavDataManager().addBatteryListener(
            new BatteryListener() {
                public void batteryLevelChanged(int i) {
                    instruments.setValueBattery(i);
                }

                public void voltageChanged(int i) {
                    instruments.setValueVoltage(i);
                }
            }
        );
    }

    private void bindAltitude(IARDrone drone) {
        drone.getNavDataManager().addAltitudeListener(
            new AltitudeListener() {
                public void receivedAltitude(int i) {
                    //tempLog(String.format("Simple Altitude: %d", i));
                }

                public void receivedExtendedAltitude(Altitude altitude) {
                    double z = altitude.getObsAlt();
                    double vz = altitude.getObsState();
                    double az = altitude.getObsAccZ();
                    // tempLog(String.format("Altitude: %s\n", altitude));
                    instruments.setValueAltitude(z);
                    instruments.setValueZSpeed(vz);
                    instruments.setValueZAccel(az);
                }
            }
        );
    }

    private void bindWind(IARDrone drone) {
        drone.getNavDataManager().addWindListener(
                (WindEstimationData d) -> instruments.setValueAirSpeed(d.getEstimatedSpeed())
        );
    }

    private void bindWifi(IARDrone drone) {
        drone.getNavDataManager().addWifiListener(
            (long link_quality) -> instruments.setValueWifi(link_quality)
        );
    }

    private void bindCompass(IARDrone drone) {
        drone.getNavDataManager().addMagnetoListener(
                (MagnetoData d) -> instruments.setValueCompass(d.getHeadingFusionUnwrapped())
        );
    }

    private void bindState(IARDrone drone) {
        drone.getNavDataManager().addStateListener(
                new StateListener() {
                    public void stateChanged(DroneState state) {
                        // tempLog(String.format("State: %s\n", state));
                    }

                    public void controlStateChanged(ControlState state) {
                        flyStatusStr = state.toString();
                    }
                }
        );
    }

    private void bindPressure(IARDrone drone) {
        drone.getNavDataManager().addPressureListener(
                new PressureListener() {
                    public void receivedKalmanPressure(KalmanPressureData d) {
                        // tempLog(String.format("Kalman Pressure: %s\n", d));
                    }
                    public void receivedPressure(Pressure d) {
                        // tempLog(String.format("Pressure: %s", d));
                    }
                }
        );
    }
    /*
    *
    *   USER INTERFACE
    *
    *
     */
    private void setupPilotProgram(String fileName) {
        //
        //  Get program from file
        //
        try {
            pilotFilename = new URL(fileName);
            String pp = String.join("\n",
                    Files.readAllLines(Paths.get(pilotFilename.getPath()),
                            StandardCharsets.UTF_8));
            pilotProg.setText(pp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void loadAgentProg() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Load Agent Program");
        fc.setInitialDirectory(new File(System.getProperty("user.dir")));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(
                "AS(L) Programs", "*.asl")
        );

        File aslFile = fc.showOpenDialog(this.stage);
        if (aslFile != null) try {
            setupPilotProgram(String.format("file://%s", aslFile.getPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void runPilotProg() {
        bootPilot();
        pilot.addPercept("start");
    }
}
