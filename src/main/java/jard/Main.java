package jard;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    Controller controller;
    AnimationTimer runStep;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("cockpit.fxml"));
        Parent root = loader.load();
        //
        //  BiDi comm stage <-> controller
        //
        controller = loader.getController();
        controller.setScene(primaryStage);
        //
        primaryStage.setTitle("J.A.R.Dr.one");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        //
        //  Control loop
        //
        runStep = new AnimationTimer() {
            public void handle(long currentNanoTime) {
                step();
            }
        };
        runStep.start();
        //
        //
        //
        primaryStage.show();
    }

    void step() {
        controller.step();
    }

    @Override
    public void stop() {
        controller.shutdown();
        try {
            super.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Application stop");
        Platform.exit();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
