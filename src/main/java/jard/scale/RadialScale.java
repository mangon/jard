package jard.scale;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;

/**
 * Created by fc on 29Mar17.
 */
public class RadialScale extends Scale {
    public static double cv(double x, double a, double b, double c, double d) {
        return c + (d - c) * (x - a) / (b - a);
    }

    protected double minVal;
    protected double maxVal;
    protected double value;


    public RadialScale(double minVal, double maxVal) {
        this(minVal, maxVal, ScaleAspect.aspect());
    }

    public RadialScale(double minVal, double maxVal, ScaleAspect aspect) {
        super(aspect);
        this.minVal = minVal;
        this.maxVal = maxVal;
        value = minVal;
    }


    public double getMinVal() {
        return minVal;
    }

    public void setMinVal(double minVal) {
        this.minVal = minVal;
    }

    public double getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(double maxVal) {
        this.maxVal = maxVal;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public void draw(GraphicsContext gc) {
        super.draw(gc);

        double startRad = cv(aspect.getPosStart(), 0, 180, 0, Math.PI);
        double endRad = cv(aspect.getPosEnd(), 0, 180, 0, Math.PI);

        gc.setStroke(aspect.getTicksColor());
        gc.setLineWidth(aspect.getTicksWidth());

        gc.setFill( aspect.getFontColor() );
        gc.setFont( aspect.getFont() );
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);

        for (int i = 0; i < aspect.getTicksNum(); i++) {
            double rad = cv(i, 0, aspect.getTicksNum() - 1, startRad, endRad);
            double c = Math.cos(rad);
            double s = Math.sin(rad);
            double x1 = aspect.getTicksStart() * c;
            double y1 = aspect.getTicksStart() * s;
            double x2 = aspect.getTicksEnd() * c;
            double y2 = aspect.getTicksEnd() * s;
            gc.strokeLine(x1, y1, x2, y2);

            if (aspect.isShowingLabels()) {
                double val = cv(rad, startRad, endRad, minVal, maxVal);
                gc.save();
                gc.scale(1.0 / 96.0, 1.0 / 96.0);
                gc.fillText(String.format(aspect.getLabelFormat(), val), 0.8 * 96 * x1, 0.8 * 96 * y1);
                gc.restore();

            }
        }

        if (aspect.isShowingLegend()) {
            gc.save();
            gc.setFill(aspect.getTicksColor());
            gc.scale(1.0 / 96.0, 1.0 / 96.0);
            gc.fillText(aspect.getLegend(), 0.0, 0.2 * 96.0);
            gc.restore();
        }

        gc.setStroke(aspect.getNeedleColor());
        gc.setLineWidth(aspect.getNeedleWidth());
        double valAngle = cv(value, minVal, maxVal, startRad, endRad);
        double c = Math.cos(valAngle);
        double s = Math.sin(valAngle);
        double x1 = aspect.getNeedleStart() * c;
        double y1 = aspect.getNeedleStart() * s;
        double x2 = aspect.getNeedleEnd() * c;
        double y2 = aspect.getNeedleEnd() * s;
        gc.strokeLine(x1, y1, x2, y2);
    }
}
