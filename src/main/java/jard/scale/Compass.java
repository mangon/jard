package jard.scale;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;

/**
 * Created by fc on 29Mar17.
 */
public class Compass extends Scale {

    double heading;

    public Compass() {
        super();
        aspect.setNeedleWidth(0.02);
        aspect.setNeedleEnd(0.5);
    }

    public Compass(ScaleAspect aspect) {
        super(aspect);
        heading = 0.0;
    }

    public void setHeading(double heading) {
        this.heading = 0.0 - heading;
    }

    @Override
    public void draw(GraphicsContext gc) {
        super.draw(gc);
        double h = RadialScale.cv(heading, 0.0, 180.0, 0.0, 180.0);
        double ns = aspect.getNeedleEnd();
        double m = 0.5 * ns * Math.sqrt(2.0);
        //
        gc.save();
        gc.rotate(h);
        gc.setStroke( aspect.getTicksColor() );
        gc.setLineWidth( aspect.getTicksWidth() );
        gc.strokeLine(-ns, 0, ns, 0);
        gc.strokeLine(0, -ns, 0, ns);
        gc.setLineWidth( 0.5 * aspect.getTicksWidth() );
        gc.strokeLine(-m, m, m, -m);
        gc.strokeLine(m, m, -m, -m);
        gc.save();
            gc.setFill( aspect.getFontColor() );
            gc.setFont( aspect.getFont() );
            gc.setTextAlign(TextAlignment.CENTER);
            gc.setTextBaseline(VPos.CENTER);
            gc.scale(1.0 / 32.0, 1.0 / 32.0);
            gc.fillText("N", 0.0, -24.0);
            gc.fillText("E", 24.0, 0.0 );
            gc.fillText("S", 0.0, 24.0);
            gc.fillText("W", -24., 0.0);
        gc.restore();
        gc.restore();
        //
        gc.beginPath();
        gc.moveTo(0.00, -.60);
        gc.lineTo(-.10, -.15);
        gc.lineTo(-.60, -.05);
        gc.lineTo(-.60, 0.10);
        gc.lineTo(-.10, 0.10);
        gc.lineTo(-.07, 0.40);
        gc.lineTo(-.25, 0.45);
        gc.lineTo(-.25, 0.50);
        gc.lineTo(0.25, 0.50);
        gc.lineTo(0.25, 0.45);
        gc.lineTo(0.07, 0.40);
        gc.lineTo(0.10, 0.10);
        gc.lineTo(0.60, 0.10);
        gc.lineTo(0.60, -.05);
        gc.lineTo(0.10, -.15);
        gc.lineTo(0.00, -.60);
        gc.closePath();
        gc.setStroke(aspect.getNeedleColor());
        gc.setLineWidth(aspect.getNeedleWidth());
        gc.stroke();
    }
}
