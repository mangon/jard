package jard.scale;

/**
 * Created by fc on 30Mar17.
 */
public class Altimeter extends RadialScale {


    public Altimeter() {
        this(ScaleAspect.aspect());
    }

    public Altimeter(ScaleAspect aspect) {
        super(0.0, 65.0, aspect);
        aspect.setShowingLabels(true);
        aspect.setShowingLegend(true);
        aspect.setLegend("Altitude(m)");
    }
}
