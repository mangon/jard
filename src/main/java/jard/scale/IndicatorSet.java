package jard.scale;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by fc on 31Mar17.
 */
public class IndicatorSet {

    static void into(GraphicsContext gc, double x, double y, double s) {
        gc.save();
        gc.translate(x, y);
        gc.scale(s, s);
    }

    RadialScale airSpeed;
    Attitude attitude;
    Altimeter altimeter;
    Turn turn;
    Compass compass;
    Speed verticalSpeed;
    Speed xSpeed;
    Speed ySpeed;
    Speed airPressure;
    Speed temperature;
    Speed battery;
    Speed wifi;

    double valueAirSpeed;
    double valueRoll;
    double valuePitch;
    double valueYaw;
    double valueAltitude;
    double valueCompass;
    double valueXSpeed, valueXAccel;
    double valueYSpeed, valueYAccel;
    double valueZSpeed, valueZAccel;
    double valueAirPressure;
    double valueTemperature;
    double valueBattery;
    double valueWifi;
    double valueWindPitch;
    double valueWindRoll;
    double valueVoltage;

    public IndicatorSet() {
        airSpeed = new RadialScale(0.0, 12.0);
        airSpeed.getAspect().setShowingLegend(true);
        airSpeed.getAspect().setLegend("Air Speed (m/s)");
        airSpeed.getAspect().setShowingLabels(true);

        attitude = new Attitude();
        altimeter = new Altimeter();
        turn = new Turn();
        compass = new Compass();

        verticalSpeed = new Speed();
        verticalSpeed.setValue(0.0);
        xSpeed = new Speed();
        xSpeed.setValue(0.0);
        xSpeed.getAspect().setLegend("Front Speed (m/s)");

        ySpeed = new Speed();
        ySpeed.setValue(0.0);
        ySpeed.getAspect().setPosStart(60.0);
        ySpeed.getAspect().setPosEnd(-240.0);
        ySpeed.getAspect().setLegend("Side Speed (m/s)");

        airPressure = new Speed();
        airPressure.getAspect().setLegend("Air Pressure (kPa)");
        airPressure.getAspect().setPosStart(330.0);
        airPressure.getAspect().setPosEnd(30.0);
        airPressure.setMinVal(0.0);
        airPressure.setMaxVal(200.0);
        airPressure.setValue(100.0);

        temperature = new Speed();
        temperature.getAspect().setLegend("Temperature (ºC)");
        temperature.getAspect().setPosStart(30.0);
        temperature.getAspect().setPosEnd(330.0);
        temperature.setMinVal(-30.0);
        temperature.setMaxVal(50.0);
        temperature.setValue(24);

        battery = new Speed();
        battery.getAspect().setShowingLegend(true);
        battery.getAspect().setLegend("Battery (%)");
        battery.getAspect().setPosStart(-240.0);
        battery.getAspect().setPosEnd(60.0);
        battery.setMinVal(0.0);
        battery.setMaxVal(100);

        wifi = new Speed();
        wifi.getAspect().setShowingLegend(true);
        wifi.getAspect().setLegend("WiFi (%)");
        wifi.getAspect().setPosStart(-240.0);
        wifi.getAspect().setPosEnd(60.0);
        wifi.setMinVal(0.0);
        wifi.setMaxVal(500.0);
    }

    public void draw(GraphicsContext gc) {
        double w = gc.getCanvas().getWidth();
        double h = gc.getCanvas().getHeight();

        double ws = 0.1 * w;
        double hs = 0.16667 * h;
        double s = 0.98 * Math.min(ws, hs);

        /*
        Six Pack
         */
        into(gc, 3 * ws, 2 * hs, s);
        airSpeed.draw(gc);
        gc.restore();

        into(gc, 5 * ws, 2 * hs, s);
        attitude.draw(gc);
        gc.restore();

        into(gc, 7 * ws, 2 * hs, s);
        altimeter.draw(gc);
        gc.restore();

        into(gc, 3 * ws, 4 * hs, s);
        turn.draw(gc);
        gc.restore();

        into(gc, 5 * ws, 4 * hs, s);
        compass.draw(gc);
        gc.restore();

        into(gc, 7 * ws, 4 * hs, s);
        verticalSpeed.draw(gc);
        gc.restore();

        /*
            Other axis speeds
         */
        into(gc, 9 * ws, 5 * hs, s);
        xSpeed.draw(gc);
        gc.restore();

        into(gc, 9 * ws, 3 * hs, s);
        ySpeed.draw(gc);
        gc.restore();
        /*
            Air Pressure
         */
        into(gc, 1 * ws, 3 * hs, s);
        airPressure.draw(gc);
        gc.restore();
        /*
            Temperature
         */
        into(gc, 1 * ws, 5 * hs, s);
        temperature.draw(gc);
        gc.restore();
        /*
            Battery
         */
        into(gc, 1 * ws, 1 * ws, s);
        battery.draw(gc);
        gc.restore();
        /*
            WiFi
         */
        into(gc, 9 * ws, 1 * ws, s);
        wifi.draw(gc);
        gc.restore();
    }

    public double getValueAirSpeed() {
        return valueAirSpeed;
    }

    public void setValueAirSpeed(double valueAirSpeed) {
        this.valueAirSpeed = valueAirSpeed;
        airSpeed.setValue(valueAirSpeed);
    }

    public double getValueRoll() {
        return valueRoll;
    }

    public void setValueRoll(double valueRoll) {
        this.valueRoll = valueRoll;
        attitude.setRoll(valueRoll);
        turn.setValue(valueRoll);
    }

    public double getValuePitch() {
        return valuePitch;
    }

    public void setValuePitch(double valuePitch) {
        this.valuePitch = valuePitch;
        attitude.setPitch(valuePitch);
    }

    public double getValueYaw() {
        return valueYaw;
    }

    public void setValueYaw(double valueYaw) {
        this.valueYaw = valueYaw;
    }

    public double getValueAltitude() {
        return valueAltitude;
    }

    public void setValueAltitude(double valueAltitude) {
        this.valueAltitude = valueAltitude;
        altimeter.setValue(valueAltitude);
    }

    public double getValueCompass() {
        return valueCompass;
    }

    public void setValueCompass(double valueCompass) {
        this.valueCompass = valueCompass;
        compass.setHeading(valueCompass);
    }

    public double getValueXSpeed() {
        return valueXSpeed;
    }

    public void setValueXSpeed(double valueXSpeed) {
        this.valueXSpeed = valueXSpeed;
        xSpeed.setValue(valueXSpeed);
    }

    public double getValueXAccel() {
        return valueXAccel;
    }

    public void setValueXAccel(double valueXAccel) {
        this.valueXAccel = valueXAccel;
    }

    public double getValueYSpeed() {
        return valueYSpeed;
    }

    public void setValueYSpeed(double valueYSpeed) {
        this.valueYSpeed = valueYSpeed;
        ySpeed.setValue(valueYSpeed);
    }

    public double getValueYAccel() {
        return valueYAccel;
    }

    public void setValueYAccel(double valueYAccel) {
        this.valueYAccel = valueYAccel;
    }

    public double getValueZSpeed() {
        return valueZSpeed;
    }

    public void setValueZSpeed(double valueZSpeed) {
        this.valueZSpeed = valueZSpeed;
        verticalSpeed.setValue(valueZSpeed);
    }

    public double getValueZAccel() {
        return valueZAccel;
    }

    public void setValueZAccel(double valueZAccel) {
        this.valueZAccel = valueZAccel;
    }

    public double getValueAirPressure() {
        return valueAirPressure;
    }

    public void setValueAirPressure(double valueAirPressure) {
        this.valueAirPressure = valueAirPressure;
        airPressure.setValue(valueAirPressure);
    }

    public double getValueTemperature() {
        return valueTemperature;
    }

    public void setValueTemperature(double valueTemperature) {
        this.valueTemperature = valueTemperature;
        temperature.setValue(valueTemperature);
    }

    public double getValueBattery() {
        return valueBattery;
    }

    public void setValueBattery(double valueBattery) {
        this.valueBattery = valueBattery;
        battery.setValue(valueBattery);
    }

    public double getValueWifi() {
        return valueWifi;
    }

    public void setValueWifi(double valueWifi) {
        this.valueWifi = valueWifi;
        wifi.setValue(valueWifi);
    }

    public double getValueWindPitch() {
        return valueWindPitch;
    }

    public void setValueWindPitch(double valueWindPitch) {
        this.valueWindPitch = valueWindPitch;
    }

    public double getValueWindRoll() {
        return valueWindRoll;
    }

    public void setValueWindRoll(double valueWindRoll) {
        this.valueWindRoll = valueWindRoll;
    }

    public double getValueVoltage() {
        return valueVoltage;
    }

    public void setValueVoltage(double valueVoltage) {
        this.valueVoltage = valueVoltage;
    }
}
