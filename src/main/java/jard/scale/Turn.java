package jard.scale;

/**
 * Created by fc on 31Mar17.
 */
public class Turn extends RadialScale {

    public Turn() {
        this(ScaleAspect.aspect());
    }

    public Turn(ScaleAspect aspect) {
        super(360, 0.0, aspect);
        aspect.setTicksNum(19);
        aspect.setTicksWidth(0.05);
        aspect.setPosStart(0.0);
        aspect.setPosEnd(360.0);
        aspect.setNeedleStart(0.75);
        aspect.setNeedleEnd(-0.75);
        aspect.setShowingLegend(true);
        aspect.setLegend("Turn Indicator");
    }
}
