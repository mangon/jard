package jard.scale;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Created by fc on 30Mar17.
 */
public class ScaleAspect {

    public static ScaleAspect aspect() {
        return new ScaleAspect();
    }

    private Color needleColor;
    private double needleWidth;
    private double needleStart;
    private double needleEnd;
    private Color ticksColor;
    private double ticksWidth;
    private double ticksStart;
    private double ticksEnd;
    private int ticksNum;

    private double posStart;
    private double posEnd;

    private Font font;

    private Color fontColor;

    private Color backgroundColor;

    private boolean showingLabels;

    private String labelFormat;

    private boolean showingLegend;
    private String legend;

    public ScaleAspect() {
        this(-225.0, 45.0);
    }

    public ScaleAspect(double posStart, double posEnd) {
        needleColor = Color.CRIMSON;
        needleWidth = 0.1;
        needleStart = 0.0; // needle DRAW range
        needleEnd = 0.6;

        ticksColor = Color.GAINSBORO;
        ticksWidth = 0.05;
        ticksStart = 0.95; // ticks line DRAW range
        ticksEnd = 1.00;
        this.posStart = posStart; // ticks set DRAW range
        this.posEnd = posEnd;
        ticksNum = 11;

        font = Font.font("monospace", FontWeight.BOLD, 16);
        fontColor = Color.KHAKI;

        backgroundColor = Color.BLACK;

        showingLabels = false;
        labelFormat = "%2.0f";

        showingLegend = false;
        legend = "";
    }

    public ScaleAspect pos(double posStart, double posEnd) {
        this.setPosStart(posStart);
        this.setPosEnd(posEnd);
        return this;
    }

    public ScaleAspect ticks(double ticksStart, double ticksEnd) {
        this.setTicksStart(ticksStart);
        this.setTicksEnd(ticksEnd);
        return this;
    }

    public ScaleAspect needle(double needleStart, double needleEnd) {
        this.setNeedleStart(needleStart);
        this.setNeedleEnd(needleEnd);
        return this;
    }


    public Color getNeedleColor() {
        return needleColor;
    }

    public void setNeedleColor(Color needleColor) {
        this.needleColor = needleColor;
    }

    public double getNeedleWidth() {
        return needleWidth;
    }

    public void setNeedleWidth(double needleWidth) {
        this.needleWidth = needleWidth;
    }

    public double getNeedleStart() {
        return needleStart;
    }

    public void setNeedleStart(double needleStart) {
        this.needleStart = needleStart;
    }

    public double getNeedleEnd() {
        return needleEnd;
    }

    public void setNeedleEnd(double needleEnd) {
        this.needleEnd = needleEnd;
    }

    public Color getTicksColor() {
        return ticksColor;
    }

    public void setTicksColor(Color ticksColor) {
        this.ticksColor = ticksColor;
    }

    public double getTicksWidth() {
        return ticksWidth;
    }

    public void setTicksWidth(double ticksWidth) {
        this.ticksWidth = ticksWidth;
    }

    public double getTicksStart() {
        return ticksStart;
    }

    public void setTicksStart(double ticksStart) {
        this.ticksStart = ticksStart;
    }

    public double getTicksEnd() {
        return ticksEnd;
    }

    public void setTicksEnd(double ticksEnd) {
        this.ticksEnd = ticksEnd;
    }

    public int getTicksNum() {
        return ticksNum;
    }

    public void setTicksNum(int ticksNum) {
        this.ticksNum = ticksNum;
    }

    public double getPosStart() {
        return posStart;
    }

    public void setPosStart(double posStart) {
        this.posStart = posStart;
    }

    public double getPosEnd() {
        return posEnd;
    }

    public void setPosEnd(double posEnd) {
        this.posEnd = posEnd;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }


    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public boolean isShowingLabels() {
        return showingLabels;
    }

    public void setShowingLabels(boolean showingLabels) {
        this.showingLabels = showingLabels;
    }

    public String getLabelFormat() {
        return labelFormat;
    }

    public void setLabelFormat(String labelFormat) {
        this.labelFormat = labelFormat;
    }


    public boolean isShowingLegend() {
        return showingLegend;
    }

    public void setShowingLegend(boolean showingLegend) {
        this.showingLegend = showingLegend;
    }

    public String getLegend() {
        return legend;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }
}
