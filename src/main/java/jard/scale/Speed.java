package jard.scale;

/**
 * Created by fc on 31Mar17.
 */
public class Speed extends RadialScale {
    public Speed() {
        this(ScaleAspect.aspect());
    }

    public Speed(ScaleAspect aspect) {
        super(5.0, -5.0, aspect);
        setValue(0.0);
        aspect.setPosStart(330.0);
        aspect.setPosEnd(30.0);
        aspect.setShowingLegend(true);
        aspect.setLegend("V Speed (m/s)");
        aspect.setShowingLabels(true);
    }
}
