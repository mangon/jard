package jard.scale;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by fc on 29Mar17.
 */
public class Scale implements IScale {

    public ScaleAspect getAspect() {
        return aspect;
    }

    public void setAspect(ScaleAspect aspect) {
        this.aspect = aspect;
    }

    protected ScaleAspect aspect;

    public Scale() {
        this(ScaleAspect.aspect());
    }

    public Scale(ScaleAspect aspect) {
        this.aspect = aspect;
    }

    public void draw(GraphicsContext gc) {
        gc.beginPath();
        gc.arc(0.0, 0.0,1.0,1.0, 0.0, 360.0);
        gc.moveTo(0.0, 0.0);
        gc.closePath();
        gc.setFill(aspect.getBackgroundColor());
        gc.fill();
    }
}
