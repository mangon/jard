package jard.scale;
import javafx.scene.canvas.GraphicsContext;

/**
 * Created by fc on 29Mar17.
 */
public interface IScale {
    void draw(GraphicsContext gc);
}
