package jard.scale;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;

/**
 * Created by fc on 31Mar17.
 */
public class LinearScale extends RadialScale {

    public LinearScale(double minVal, double maxVal) {
        this(minVal, maxVal, ScaleAspect.aspect());
    }

    public LinearScale(double minVal, double maxVal, ScaleAspect aspect) {
        super(minVal, maxVal, aspect);
        aspect.setTicksStart(-0.9);
        aspect.setTicksEnd(0.70);
        aspect.setNeedleStart(-0.85);
        aspect.setNeedleEnd(0.65);
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setFill(aspect.getBackgroundColor());
        gc.fillRect(-1.0, -1.0, 2.0, 2.0);

        double startPos = -1.0;
        double endPos = 1.0;

        gc.setStroke(aspect.getTicksColor());
        gc.setLineWidth(aspect.getTicksWidth());

        gc.setFill( aspect.getFontColor() );
        gc.setFont( aspect.getFont() );
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setTextBaseline(VPos.CENTER);

        for (int i = 0; i < aspect.getTicksNum(); i++) {
            double x = cv(i, 0, aspect.getTicksNum() - 1, startPos, endPos);
            double y1 = aspect.getTicksStart();
            double y2 = aspect.getTicksEnd();
            gc.strokeLine(x, y1, x, y2);

            if (aspect.isShowingLabels()) {
                double val = cv(x, startPos, endPos, minVal, maxVal);
                gc.save();
                gc.scale(1.0 / 96.0, 1.0 / 96.0);
                gc.fillText(
                        String.format(aspect.getLabelFormat(), val),
                        0.8 * 96 * x, 0.8 * 96 * y1);
                gc.restore();
            }
        }

        if (aspect.isShowingLegend()) {
            gc.save();
            gc.setFill(aspect.getTicksColor());
            gc.scale(1.0 / 96.0, 1.0 / 96.0);
            gc.fillText(aspect.getLegend(), 0.0, 0.875 * 96.0);
            gc.restore();
        }

        gc.setFill(aspect.getNeedleColor());
        double val = cv(value, minVal, maxVal, startPos, endPos);
        double y1 = aspect.getNeedleStart();
        double y2 = aspect.getNeedleEnd();
        gc.fillRect(startPos, y1, val - startPos, y2 - y1);
    }
}
