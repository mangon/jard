package jard.scale;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Created by fc on 30Mar17.
 */
public class Attitude extends Scale {

    double pitch;
    double roll;

    public Attitude() {
        this(ScaleAspect.aspect());
    }

    public Attitude(ScaleAspect aspect) {
        super(aspect);
    }

    public double getPitch() {
        return pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    public double getRoll() {
        return roll;
    }

    public void setRoll(double roll) {
        this.roll = roll;
    }

    public double deltaPitch() {
        return RadialScale.cv(pitch, -180.0, 180.0, -4.0, 4.0);
    }

    @Override
    public void draw(GraphicsContext gc) {
        super.draw(gc);
        gc.save();
            gc.beginPath();
                gc.arc(0.0, 0.0, 1.0, 1.0, 0.0, 360.0);
            gc.closePath();
            gc.clip();
            gc.save();
                gc.translate(0, deltaPitch());
                gc.rotate(roll);
                gc.setFill(Color.DEEPSKYBLUE);
                gc.fillRect(-10.0, -10.0, 20.0, 10.0);
                gc.setFill(Color.SEAGREEN);
                gc.fillRect(-20.0, 0.0, 40.0, 20.0);
            gc.restore();
            //
            gc.setStroke(aspect.getNeedleColor());
            gc.setLineWidth(aspect.getNeedleWidth());
            gc.beginPath();
                gc.moveTo(-1, 0.0);
                gc.lineTo(-0.5, 0.0);
                gc.lineTo(-0.25, 0.25);
                gc.lineTo(0.25, 0.25);
                gc.lineTo(0.5, 0.0);
                gc.lineTo(1, 0.0);
                gc.moveTo(-1, 0.0);
            gc.closePath();
            gc.stroke();
        gc.restore();
    }
}
